from django.urls import path
from accounts.views import login_account, logout_account, signup_account


urlpatterns = [
    path("login/", login_account, name="login"),
    path("logout/", logout_account, name="logout"),
    path("signup/", signup_account, name="signup")
]
